import os
import socket
import logging
import sys
import argparse
import yaml
import httplib2

from common import write_list
from common import read_list


def parse_arguments():
    parser = argparse.ArgumentParser("Checks if urls in list are relevant.")
    parser.add_argument("input", help="Input List.")
    parser.add_argument("output", help="Output List.")
    args = parser.parse_args()
    return args


def check_exists(item):
    prefixes = ["http://", "https://"]
    item = item.replace(prefixes[0], "").replace(prefixes[1], "")
    connection = httplib2.Http()
    for entry in prefixes:
        try:
            response = connection.request(entry + item, 'HEAD')
            if int(response[0]['status']) < 300:
                return True
        except:
            continue
    return False


def main(args):
    url_list = read_list(args.input)
    result_list = []
    for item in url_list:
        if check_exists(item):
            result_list.append(item)

    write_list(result_list, args.output)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)

