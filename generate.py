import os
import socket
import logging
import sys
import argparse
import yaml
import httplib2

def check_exists(item):
    logging.warn("Check item: {}".format(item))
    h = httplib2.Http()
    try:
        if not item.startswith("http://"):
            item = "http://" + item
        resp = h.request(item, 'HEAD')
        if int(resp[0]['status']) < 400:
            logging.warn("Exists")
            return True
        return False
    except Exception as e:
        return False

def write(item, hosts, used_set):
    if not check_exists(item):
        return

    item = item.strip()
    if item not in used_set and len(item) > 0:
        used_set.add(item)
        hosts.write("0.0.0.0\t" + item + "\n")


def parse_search_yml():
    with open("./yml-search/SearchEngines.yml", "r") as stream:
        obj = yaml.safe_load(stream)
    return obj



def read_suffix_list():
    result = []
    with open("tld-list-basic.txt", "r") as suffixes:
        for line in suffixes:
            result.append(line.strip())
    return result


def process_yaml_obj(obj, hosts, used_set, suffix_list, pre_domain_list):
    for engine, storage in obj.items():
        for item in storage:
            if 'urls' not in item:
                continue
            for url in item['urls']:
                if '{}' not in url:
                    write(url, hosts, used_set)
                else:
                    for suffix in suffix_list:
                        fixed = url.format(suffix)
                        if "{}" in fixed:
                            raise ValueError("Two {} in url")
                        write(fixed, hosts, used_set)
                        for pre_domain in pre_domain_list:
                            ending = pre_domain + "." + suffix
                            write(url.format(ending), hosts, used_set)


def ban_listed_porn(hosts, used_set):
    for item in os.listdir("porn"):
        for child in os.listdir("porn/" + item):
            with open("porn/" + item + "/" + child, "r") as stream:
                for line in stream:
                    if len(line.strip()) > 0:
                        write(line.strip(), hosts, used_set)



def main():
    PREFIX_LIST = [
        ]

    DOMAIN_LIST  = [
            'google', 'yandex', 'ya', 'mail', 'bing', 'yahoo', 'baidu', 'duckduckgo', 'vk', 'rambler', "tut",
            "btdigg", "isohunt", "mininova", "thepiratebay", "torrentz", "torrentspy", "rutracker", "gibiru",
            "walla", "seznam", "sesam", "search", "sapo", "bbc", "cbc", "forbes",
    ]

    OUT_TMP = [
            "aol", "ask", "dogpile", "excite", "webcrawler", "yippi",
            "ecosia", "exalead", "gigablast", "hotbot", "lycos", "metacrawler", "mojeek", "qwant", "searx",
            "sogou", "soso", "startpage", "swisscows", "youdao", "najdi", "ziplocal",
            "rediff", "pipilika", "naver", "najdi", "miner", "maktoob",
            "leit", "goo", "fireball", "egerin", "dauum", "biglobe", "acoona",
    ]


    PRE_DOMAIN = ['com', 'co']
    SUFFIX_LIST = read_suffix_list()

    hosts = open("hosts", "w")
    used_set = set()
    ban_host_porn(hosts, used_set)
    ban_host_news(hosts, used_set)


    yml_search = parse_search_yml()
    yml_social = parse_social_yml()

    process_yaml_obj(yml_search, hosts, used_set, SUFFIX_LIST, PRE_DOMAIN)
    process_yaml_obj(yml_social, hosts, used_set, SUFFIX_LIST, PRE_DOMAIN)

    ban_listed_porn(hosts, used_set)

    for suffix in SUFFIX_LIST:
        for domain in set(DOMAIN_LIST):
            write(domain + "." +suffix, hosts, used_set)
            for item in PRE_DOMAIN:
                write(domain + "." + item + "." + suffix, hosts, used_set)
            for pref in PREFIX_LIST:
                write(pref + "." + domain + "." + suffix, hosts, used_set)
                for item in PRE_DOMAIN:
                    write(pref + "." + domain + "." + item + "." + suffix, hosts, used_set)
    hosts.close()


if __name__ == '__main__':
    main()
