#!/usr/bin/env python3
import argparse
from common import read_list


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="File with urls.")
    parser.add_argument("hosts", help="Where to put output file.")
    args = parser.parse_args()
    return args


def write_hosts(item, hosts):
    item = item.strip()
    if len(item) > 0:
        hosts.write("0.0.0.0\t" + item + "\n")


def main(args):
    line_list = read_list(args.input)
    with open(args.hosts, "w") as hosts:
        for item in line_list:
            write_hosts(item, hosts)


if __name__ == "__main__":
    args = parse_arguments()
    main(args)


