#!/usr/bin/env python3
import yaml
import argparse

from suffix import Suffixes
from common import read_yaml
from common import write_list


def parse_arguments():
    parser = argparse.ArgumentParser(
            "For yaml socials.")
    parser.add_argument("input", help="Input file in yaml.")
    parser.add_argument("output", help="Output file.")
    args = parser.parse_args()
    return args


def main(args):
    yaml_info = read_yaml(args.input)
    result_list = []
    for key, array_list in yaml_info.items():
        result_list += array_list
    write_list(result_list, args.output)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)


