#!/usr/bin/env python3
import argparse

from common import read_list
from common import write_list


def parse_arguments():
    parser = argparse.ArgumentParser(
            "Filters allowed domains.")
    parser.add_argument("input", help="File with all domains.")
    parser.add_argument("allowed", help="File with allowed domains.")
    parser.add_argument("output", help="Output file.")
    args = parser.parse_args()
    return args


def main(args):
    allowed_list = read_list(args.allowed)
    allowed_set = set(allowed_list)

    host_lines = []
    with open(args.input) as input_hosts:
        for line in input_hosts:
            if len(line.strip()) == 0 or line.strip().startswith("#"):
                host_lines.append(line)
            else:
                tokens = line.strip().split()
                good = True
                for item in allowed_set:
                    if item in tokens[1]:
                        good = False
                if good:
                    host_lines.append(line)

    with open(args.output, "w") as output_hosts:
        for line in host_lines:
            output_hosts.write(line)


if __name__ == "__main__":
    args = parse_arguments()
    main(args)

