import yaml
import argparse

from suffix import Suffixes
from common import read_yaml
from common import process_yaml_info
from common import write_list


def parse_arguments():
    parser = argparse.ArgumentParser(
            "For yaml search and socials.")
    parser.add_argument("input", help="Input file in yaml.")
    parser.add_argument("output", help="Output file.")
    args = parser.parse_args()
    return args


def main(args):
    yaml_info = read_yaml(args.input)
    result_list = process_yaml_info(yaml_info)
    write_list(result_list, args.output)


if __name__ == '__main__':
    args = parse_arguments()
    main(args)


