#!/usr/bin/env bash

allowed="allowed.in"
allowed_personal="allowed.personal.in"

set -uexo pipefail

cook_dir=cook_nekrald_hosts
rm -rf $cook_dir
mkdir -p $cook_dir

git submodule update --init --recursive
python3 personal.py lists/personal.in $cook_dir/personal.out
python3 search.py lists/search.yml $cook_dir/search.out
python3 social.py lists/social.yml $cook_dir/social.out
cp lists/head.in $cook_dir/head.out
cp lists/ads/cares.in $cook_dir/cares.out
cp lists/ads/bbh.in $cook_dir/bbh.out
cp lists/adult/clefspeare.in $cook_dir/clefspeare.out
cp lists/adult/sinfonietta.in $cook_dir/sinfonietta.out
cp lists/adult/tiuxo.in $cook_dir/tiuxo.out
cp lists/news/news.in $cook_dir/news.out
cp lists/search/adult-google.in $cook_dir/adult-google.out
cp lists/search/simulator-google.in $cook_dir/simulator-google.out
cat $cook_dir/simulator-google.out $cook_dir/adult-google.out $cook_dir/news.out $cook_dir/clefspeare.out $cook_dir/sinfonietta.out $cook_dir/tiuxo.out $cook_dir/bbh.out $cook_dir/cares.out $cook_dir/personal.out $cook_dir/search.out $cook_dir/social.out  > $cook_dir/merge.out
sort $cook_dir/merge.out | uniq > $cook_dir/result.out

cp $cook_dir/result.out $cook_dir/exist.out
# python3 existence.py $cook_dir/result.out $cook_dir/exist.out

python3 hosts.py $cook_dir/exist.out $cook_dir/hosts.nekrald
python3 filter.py $cook_dir/hosts.nekrald lists/$allowed $cook_dir/hosts.filtered.temp
python3 filter.py $cook_dir/hosts.filtered.temp lists/$allowed_personal $cook_dir/hosts.filtered
cat $cook_dir/head.out $cook_dir/hosts.filtered > $cook_dir/hosts.final

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    sudo cp $cook_dir/hosts.final /etc/hosts
    ./update_ubuntu.sh
elif [[ "$OSTYPE" == "darwin"* ]]; then
    sudo cp $cook_dir/hosts.final /private/etc/hosts
    ./update_mac.sh
fi


set +uexo pipefail
