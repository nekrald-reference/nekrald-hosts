#!/usr/bin/env python3
""" This script transforms the list of personal hosts. """

import argparse
import sys
import os
from typing import List

from suffix import Suffixes
from prefix import Prefixes
from common import write_list
from common import read_list


class ReplaceKeys:
    SFX: str = 'SFX'
    PFX: str = 'PFX'


def parse_arguments() -> argparse.Namespace:
    parser: argparse.ArgumentParser = argparse.ArgumentParser(
            "Create list of web addresses.")
    parser.add_argument("input", help="Input file with patterns.")
    parser.add_argument("output", help="Output file.")
    args: argparse.Namespace = parser.parse_args()
    return args


def provide_replacement(
        line_list: List[str], key: str, replace_list: List[str]) -> List[str]:
    result_list: List[str] = []
    replacement: str
    transform: dict = {
        ReplaceKeys.SFX: '{' + ReplaceKeys.SFX + '}',
        ReplaceKeys.PFX: '{' + ReplaceKeys.PFX + '}',
    }
    for replacement in replace_list:
        transform[key] = replacement
        entry: str
        for entry in line_list:
            result_list.append(entry.format(**transform))
    return result_list


def provide_suffixes(line_list: List[str]) -> List[str]:
    return provide_replacement(line_list,
            ReplaceKeys.SFX, Suffixes.SHORT_SUFFIXES)


def provide_prefixes(line_list: List[str]) -> List[str]:
    return provide_replacement(line_list,
            ReplaceKeys.PFX, Prefixes.PREFIX_LIST)


def main(args: argparse.Namespace):
    result_items: List[str] = []

    for src_line in read_list(args.input):
        sources = [src_line]
        if not src_line.startswith("www."):
            sources.append( "www." + src_line)
        else:
            sources.append(src_line[4:])
        for line in sources:
            items_for_out = [line]
            if "{" + ReplaceKeys.SFX + "}" in line:
                items_for_out = provide_suffixes(items_for_out)
            if "{" + ReplaceKeys.PFX + "}" in line:
                items_for_out = provide_prefixes(items_for_out)
            result_items += items_for_out

    write_list(result_items, args.output)


if __name__ == "__main__":
    args = parse_arguments()
    main(args)
