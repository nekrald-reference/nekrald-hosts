import yaml
from suffix import Suffixes


def read_yaml(file_path):
    with open(file_path, "r") as stream:
        obj = yaml.safe_load(stream)
    return obj


def process_yaml_info(obj):
    result_list = []
    for engine, storage in obj.items():
        for item in storage:
            if 'urls' not in item:
                continue
            for url in item['urls']:
                if '{}' not in url:
                    result_list.append(url)
                    result_list.append("search." + url)
                    result_list.append("websearch." + url)
                else:
                    for suffix in Suffixes.SHORT_SUFFIXES:
                        fixed = url.format(suffix)
                        if "{}" in fixed:
                            raise ValueError("Two {} in url.")
                        result_list.append(fixed)
                        result_list.append("search." + fixed)
                        result_list.append("websearch." + fixed)
    return result_list


def write_list(url_list, output_path):
    with open(output_path, "w") as output_file:
        for line in url_list:
            output_file.write(line + "\n")


def read_list(input_path):
    line_list = []
    with open(input_path, "r") as input_file:
        for line in input_file:
            line = line.strip()
            if line.startswith("#") or len(line) == 0:
                continue
            line_list.append(line)
    return line_list



